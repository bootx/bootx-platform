package cn.bootx.platform.baseapi.service.chinaword;


import cn.bootx.platform.baseapi.dao.chinaword.ChinaWordManager;
import cn.bootx.platform.baseapi.entity.chinaword.ChinaWord;
import cn.bootx.platform.baseapi.handler.chinaword.WordContext;
import cn.bootx.platform.baseapi.handler.chinaword.WordFilter;
import cn.bootx.platform.baseapi.handler.chinaword.WordType;
import cn.bootx.platform.baseapi.param.chinaword.ChinaWordParam;
import cn.bootx.platform.baseapi.result.chinaword.ChinaWordResult;
import cn.bootx.platform.baseapi.result.chinaword.ChinaWordVerifyResult;
import cn.bootx.platform.common.mybatisplus.util.MpUtil;
import cn.bootx.platform.core.exception.DataNotExistException;
import cn.bootx.platform.core.rest.param.PageParam;
import cn.bootx.platform.core.rest.result.PageResult;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 敏感词服务
 * @author xxm
 * @since 2023/8/9
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ChinaWordService {
    private final WordContext wordContext;
    private final WordFilter wordFilter;

    private final ChinaWordManager chinaWordManager;

    /**
     * 添加
     */
    public void add(ChinaWordParam param){
        ChinaWord chinaWord = ChinaWord.init(param);
        this.updateWord(chinaWord);
        chinaWordManager.save(chinaWord);

    }

    /**
     * 修改
     */
    public void update(ChinaWordParam param){
        ChinaWord ChinaWord = chinaWordManager.findById(param.getId()).orElseThrow(DataNotExistException::new);
        BeanUtil.copyProperties(param,ChinaWord, CopyOptions.create().ignoreNullValue());
        refresh();
        chinaWordManager.updateById(ChinaWord);
    }

    /**
     * 分页
     */
    public PageResult<ChinaWordResult> page(PageParam pageParam, ChinaWordParam query){
        return MpUtil.toPageResult(chinaWordManager.page(pageParam,query));
    }

    /**
     * 获取单条
     */
    public ChinaWordResult findById(Long id){
        return chinaWordManager.findById(id).map(ChinaWord::toResult).orElseThrow(DataNotExistException::new);
    }

    /**
     * 获取全部
     */
    public List<ChinaWordResult> findAll(){
        return MpUtil.toListResult(chinaWordManager.findAll());
    }

    /**
     * 删除
     */
    public void delete(Long id){
        refresh();
        chinaWordManager.deleteById(id);
    }

    /**
     * 测试敏感词效果
     */
    public ChinaWordVerifyResult verify(String text, int skip, char symbol){
        ChinaWordVerifyResult result = new ChinaWordVerifyResult();
        if (wordFilter.include(text,skip)) {
            String replaceText = wordFilter.replace(text, skip, symbol);
            int count = wordFilter.wordCount(text, skip);
            result.setText(replaceText)
                    .setCount(count)
                    .setSensitive(true);
        }
        return result;
    }

    /**
     * 刷新缓存
     */
    public void refresh(){
        initData();
    }

    /**
     * 更新敏感词库
     */
    public void updateWord(ChinaWord chinaWord){
        if (Objects.equals(chinaWord.getEnable(),true)){
            if (Objects.equals(chinaWord.getWhite(),true)){
                wordContext.addWord(Collections.singleton(chinaWord.getWord()), WordType.WHITE);
            } else {
                wordContext.addWord(Collections.singleton(chinaWord.getWord()), WordType.BLACK);
            }
        }
    }

    /**
     * 初始化数据
     */
    @Async
    @EventListener(WebServerInitializedEvent.class)
    public void initData(){
        List<ChinaWord> chinaWords = chinaWordManager.findAllByEnable(true);
        // 黑名单
        Set<String> black = chinaWords.stream()
                .filter(o -> Objects.equals(o.getWhite(), false))
                .map(ChinaWord::getWord)
                .collect(Collectors.toSet());
        // 白名单
        Set<String> white = chinaWords.stream()
                .filter(o -> Objects.equals(o.getWhite(), true))
                .map(ChinaWord::getWord)
                .collect(Collectors.toSet());

        wordContext.initKeyWord(black,white);
    }
}

