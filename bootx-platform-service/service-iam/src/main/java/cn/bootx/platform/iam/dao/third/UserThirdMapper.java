package cn.bootx.platform.iam.dao.third;

import cn.bootx.platform.iam.entity.third.UserThird;
import com.github.yulichang.base.MPJBaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 三方登录
 *
 * @author xxm
 * @since 2021/8/2
 */
@Mapper
public interface UserThirdMapper extends MPJBaseMapper<UserThird> {

}
